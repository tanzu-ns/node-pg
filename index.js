const express = require('express');
const { Pool } = require('pg');

const app = express();
const port = process.env.PORT || 3000;

const serviceBindings = require('kube-service-bindings');

try {
  // check if the app has been bound to a Postgres instance through
  // service bindings. If so use that connect info
  pgBinding = serviceBindings.getBinding('POSTGRESQL', 'pg');
} catch (err) { 
  console.log(err);
};

const dbOptions = {
  user: pgBinding.user || process.env.DB_USER,
  host: pgBinding.host || process.env.DB_HOST,
  database: pgBinding.database || process.env.DB_NAME,
  password: pgBinding.password || process.env.DB_PASSWORD,
  port: pgBinding.port || process.env.DB_PORT,
  preparedStatements: false
};

const pool = new Pool(dbOptions);

const seedUsers = async () => {
  const createTableQuery = `
    CREATE TABLE IF NOT EXISTS users (
      id SERIAL PRIMARY KEY,
      name VARCHAR(255) NOT NULL
    )
  `;
  await pool.query(createTableQuery);
  const countQuery = 'SELECT COUNT(*) FROM users';
  const countResult = await pool.query(countQuery);
  const userCount = parseInt(countResult.rows[0].count);

  // Only insert users if the table is empty
  if (userCount === 0) {
    const names = ['Alice', 'Bob', 'Charlie', 'David', 'Eve'];
    const insertQuery = 'INSERT INTO users (name) VALUES ($1)';

    for (const name of names) {
      const query = {
        text: insertQuery,
        values: [name],
      };
      await pool.query(query);
    }
  }
};

// Function to get all users from the DB
const getUsers = async () => {
  const query = 'SELECT * FROM users';
  const result = await pool.query(query);
  return result.rows;
};

// Call seedUsers on app startup
(async () => {
try {
    await seedUsers();
    console.log('Users seeded successfully');
  } catch (err) {
    console.error('Error seeding users:', err);
    process.exit(1);
  }
})();

app.get('/', async (req, res) => {
  const users = await getUsers();
  res.send(`
    <html>
      <body>
        <h1>Users:</h1>
        <ul>
          ${users.map((user) => `<li>${user.name}</li>`).join('')}
        </ul>
      </body>
    </html>
  `);
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

module.exports = app;
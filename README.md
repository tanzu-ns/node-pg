# Node PG

A Node.js app which uses Postgres to display a list of users to a web page.

### Deploying to TAP
Create a [service claim](https://docs.vmware.com/en/VMware-Tanzu-Application-Platform/1.5/tap/getting-started-claim-services.html) on a Postgres database.  Then take note of the `Claim Reference` in the output of `tanzu service class-claim get my-postgres`.  Where "my-postgres" is what you called the PG instance.  

Then you can update the workload file, with this reference and `tanzu app workload create -f workload.yml`

### Airgapped Notes
When running this airgapped, add an `.npmrc` to this repo for the buildpack to pick up.  You can set this with `npm config set registry https://npm.example.com` and then copying the file in here.  It should look like this.

```
registry=https://npm.example.com/
```

You might also need to add a custom CA for the above registry.  These can also go into the `.npmrc` if needed by using the `ca` or `cafile` options of npm.  E.g. `npm config set cafile "/home/user/my-ca/ca.crt"`